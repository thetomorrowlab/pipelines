# Modular Laravel

## Introduction
***

This app uses docker-compose to build everything, so its all you need to install to run it.

The app is built ontop of the Laravel framework for backend support (using a modular architecture), and Angular Material for frontend support.

*** Note that while this app is opinionated, feel free to rip out angular and use the standard laravel architecture, really this repo is more about showcasing docker and bitbucket pipelines.***


## Installing Docker Compose
***

Docker and the associated package management tool, docker-compose are now widely supported across all major platforms. To install simply follow the instructions [here][docker_install].


## Getting started
***

First you will need to create a `.env` file in the `app/` dir, following the same structure as the `.env.example` file, filling in the blanks where applicable. You can find the development values in passpack.

With [Docker][docker] installed, to start your application should be as simple
as:

    $ make run

This will start all the services required for the app, as well as provision the frontend build assets.

To load in all the laravel database migrations, as well as the initial database data, while the app is running, run:

    $ make laravel-provision

Everything is now setup, and you can visit the running application at [https://localhost:8080](https://localhost:8080).

The laravel migration will create an admin user with the details:

    Name:       admin
    Email:      admin@thetomorrowlab.com
    Password:   admin

While the container is up, all changes on the frontend app will also be monitored through `browserify`. So your changes to the app and build files will always be reflected in the running development server.

To view all available make commands, simply run `make` inside your terminal at the root of this project.

*** Note that all make `shell` commands and `laravel-provision` `test-be` are specific to this build, and should be updated to reflect your project folder name ***

## CI
***
Currently there is a `bitbucket-pipelines.yml` file in the root of the repo. This will trigger any pushes onto bitbucket to trigger a CI build, if you have pipelines enabled.

You can see the CI docker image at `/ops/ci`.

## Deployment
***
With CI enabled, you can setup automated deploys with [source forge](https://forge.laravel.com).

Firstly setup the server on source forge.
You will need to setup some build scripts to prepare your server to build the app.

At The Tomorrow Lab, we have packaged all of our build sets in a `Recipe` on source forge, and can simply select that to run post build. ("All work for preparing new sites in one script").
_In the future would be good to expose that script publicily_

Once the server has been provisioned, there are a few steps before it is ready.


### Sites
The first screen you will come to is the "Sites" page. Here you can app your server to an IP address, and tell it where to expect you app to be located.

Assuming you have setup your server to be deployed to Digital Ocean, head over to their [website](https://cloud.digitalocean.com/), and create you domain.
You can setup subdomains under the "Networking" tab, creating A records for each sub domain.
_(You can find the IP address at the top right of the source forge server control panel)_

Once you create the A record for your new app, copy the domain across into the laravel forge "Root Domain" field.
Leave the "Project Type" as is

Set the "Web Directory" to `/app/public`.

And save. This should create a new row under the "Active Sites" at the bottom of the page.
_(Feel free to remove the `default` site.)_

### MySQL

Create a MySql database from the server control panel on source forge, you should see this on the left hand nav menu.
The database name and login details you create here will eventually need to be mapped in the `.env` file.

### Site Details - Apps

Connect your laravel forge server to your git repository, using the wizard on "Site Details" "Apps" page.
Uncheck the `Install Composer Dependencies`, as we are going to handle that ourselves in the push steps.

Once finished you should be prompted with a number of new fields. Edit the "Deploy Script" field, and replace it with:

    cd /home/forge/ci-playground.do-ttl-stage.com
    git pull origin master
    cd frontend && make build
    cd ../app && make run-deploy-scripts
    echo "" | sudo -S service php7.1-fpm reload

Below this field should be the "Deployment Trigger URL". Copy the key parts into git pipelines env

`https://forge.laravel.com/servers/{LARAVEL_FORGE_ID}/sites/{LARAVEL_FORGE_SERVER_ID}/deploy/http?token={LARAVEL_FORGE_SITE_TOKEN}`

    Settings -> Pipelines -> Environment variables


 and save it "Secured".

### Site Details - Environment

The final step is to setup your production `.env` file. Click on "Environment" on the left side panel, and click "EDIT ENVIRONMENT", and copy in all of your required environment values, including the details you added for the MySQL table created eariler.

### CD & CI
Most deployments are automatically handled through Continuous Deployment with Continuous Integration tests.

For example when working on a branch `feature/my-change` when you commit your code, CI will run through all the tests, and if successful will deploy to the staging environment.

Similarily pushing changes to a `release/*` branch, will deploy to the release server (if tests pass).

Production deployments should always be a very purposeful action, and therefore requires manual deployment via [forge](https://forge.laravel.com/servers/152308).

## GitFlow
This project flows the basic gitflow standard of:

    Master
        release/*
            feature/*
                task/*

All feature and task branches should include the jira ticket ID, that it relates to. This allows us to easily relate branches to tasks.
**All changes should be logged in jira**


## Database

App uses the standard MariaDB that comes with forge servers.

## Done

That everything, all your changes should now be tracked with CI with bitbucket pipelines, and successful builds should kick of CD on laravel forge, to update the deployed app.


[docker_install]: https://docs.docker.com/compose/install/  "Docker Installation"
