# Show command line help message
help:
	@echo "Dockerised Larvel for [App Name]"
	@echo ""
	@echo "The following commands are available:"
	@echo ""
	@echo "    make run:                    Runs all the developement containers to host your app."
	@echo "    make run-silent:             Runs compose in detached mode."
	@echo "    make fe-provision:           Builds out all frontend assets into the public folder."
	@echo "    make laravel-provision:      Runs database migrations and composer install."
	@echo "    make test-be:                Run application's backend tests."
	@echo "    make web-shell:              Launch a Bash shell inside the web container (container must already be running)."
	@echo "    make php-shell:              Launch a Bash shell inside the php container (container must already be running)."
	@echo "    make fe-shell:               Launch a Bash shell inside the fe container (container must already be running)."
	@echo "    make db-shell:               Launch a Bash shell inside the db container (container must already be running)."
	@echo ""
	@echo "    make stop-all:               Stops all running docker containers (Warning! This is a global command)."
	@echo "    make remove-all:             Removes all stopped docker containers (Warning! This is a global command)."

run:
	docker-compose up

run-silent:
	docker-compose up -d

fe-provision:
	docker-compose run frontend make build

laravel-provision:
	docker exec -i -t pipelines_php_1 /bin/bash -c "make provision"

test-be:
	docker exec -i -t pipelines_php_1 /bin/bash -c "make test"

test-ci:
	docker-compose -f docker-compose-ci.yml up

web-shell:
	docker exec -i -t pipelines_web_1 /bin/bash

php-shell:
	docker exec -i -t pipelines_php_1 /bin/bash

fe-shell:
	docker exec -i -t pipelines_frontend_1 /bin/bash

db-shell:
	docker exec -i -t pipelines_db_1 /bin/bash

stop-all:
	docker ps | awk 'NR>1 {print $1}' | xargs docker stop

remove-all:
	docker ps -a | awk 'NR>1 {print $1}' | xargs docker rm
