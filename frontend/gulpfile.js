var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var browserify = require('browserify');
var source = require('vinyl-source-stream');

gulp.task('browserify', function () {
    'use strict';
    // Grabs the app.js file
    browserify('./app/public/controller.js')
        // bundles it and creates a file called main.js
        .bundle()
        .pipe(source('main.js'))
        // saves it the public/js/ directory
        .pipe(gulp.dest('../app/public/js/'));

    browserify('./app/admin/controller.js')
        .bundle()
        .pipe(source('admin.js'))
        .pipe(gulp.dest('../app/public/js/'));
});

// Bundle the angular material library components into a single vendor file
gulp.task('vendor', function () {
    'use strict';
    gulp.src([
        './node_modules/angular/angular.js',
        './node_modules/angular-aria/angular-aria.js',
        './node_modules/angular-animate/angular-animate.js',
        './node_modules/angular-material/angular-material.js'
    ])
        .pipe(concat('vendor.js'))
        .pipe(uglify())
        .pipe(gulp.dest('../app/public/js/'));

    gulp.src('./node_modules/angular-material/angular-material.min.css')
        .pipe(gulp.dest('../app/public/css/'));
});

gulp.task('watch', function () {
    'use strict';
    gulp.watch('app/scripts/**/*.js', ['browserify']);
});

gulp.task('build', ['browserify', 'vendor']);
gulp.task('default', ['watch']);
