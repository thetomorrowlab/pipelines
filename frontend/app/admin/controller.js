var app = angular.module('AdminApp', ['ngMaterial']);

// Syntax changed to prevent conflicts with blade templates
app.config(['$interpolateProvider', function ($interpolateProvider) {
    'use strict';
    $interpolateProvider.startSymbol('{[');
    $interpolateProvider.endSymbol(']}');
}]);


app.controller('MainCtrl', function ($scope, $mdSidenav) {
    'use strict';

    $scope.toggleSideBar = function () {
        $mdSidenav('left').toggle();
    };

    $scope.goTo = function (path) {
        window.location = path;
    };
});

app.controller('ListCtrl', function ($scope, $http) {
    'use strict';

    $scope.$watch('apiPath', function () {
        $http.get($scope.apiPath).then(function successCallback(response) {
            $scope.data = response.data.data;
        }, function errorCallback(response) {
            console.log(response);
        });
    });
});
