var app = angular.module('PublicApp', ['ngMaterial']);

// Syntax changed to prevent conflicts with blade templates
app.config(['$interpolateProvider', function ($interpolateProvider) {
    'use strict';
    $interpolateProvider.startSymbol('{[');
    $interpolateProvider.endSymbol(']}');
}]);


app.controller('MainCtrl', function ($scope, $mdSidenav) {
    'use strict';

    $scope.goTo = function (path) {
        window.location = path;
    };
});
