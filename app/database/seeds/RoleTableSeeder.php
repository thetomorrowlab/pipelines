<?php

use Illuminate\Database\Seeder;
use App\Modules\Auth\Models\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Sets up the site roles for ACL
     *
     * @return void
     */
    public function run()
    {
        if (!Role::where('name', 'user')->count()) {
            $role_employee = new Role();
            $role_employee->name = 'user';
            $role_employee->description = 'Access to authenticated services';
            $role_employee->save();
        }

        if (!Role::where('name', 'admin')->count()) {
            $role_manager = new Role();
            $role_manager->name = 'admin';
            $role_manager->description = 'Full access';
            $role_manager->save();
        }
    }
}
