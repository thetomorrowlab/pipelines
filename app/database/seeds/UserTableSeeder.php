<?php

use Illuminate\Database\Seeder;
use App\Modules\Auth\Models\Role;
use App\Modules\Auth\Models\User;

class UserTableSeeder extends Seeder
{
    /**
     * Setup a admin user
     * Should only be run on test instances and not production
     *
     * @return void
     */
    public function run()
    {
        if (!User::where('email', 'admin@thetomorrowlab.com')->count()) {
            $adminRole = Role::where('name', 'admin')->first();

            $admin = new User();
            $admin->name = 'admin';
            $admin->email = 'admin@thetomorrowlab.com';
            $admin->password = bcrypt('admin');
            $admin->save();
            $admin->roles()->attach($adminRole);
        }
    }
}
