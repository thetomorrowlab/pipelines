<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

/**
* ModuleViewServiceProvider
*
* The service provider for the modules. After being registered
* it will make sure that each of the modules are properly loaded views.
*/
class ModuleViewServiceProvider extends ServiceProvider
{
    /**
     * Will make sure that the required modules have been fully loaded
     * @return void
     */
    public function boot()
    {
        // For each of the registered modules, include their routes and Views
        $modules = config("module.modules");

        while (list(,$module) = each($modules)) {

            // Load the views
            if (is_dir(__DIR__.'/../Modules/'.$module.'/Views')) {
                $this->loadViewsFrom(__DIR__.'/../Modules/'.$module.'/Views', $module);
            }
        }
    }
}
