<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider;
use Illuminate\Routing\Router;


/**
* ModuleRouteServiceProvider
*
* The service provider for the modules. After being registered
* it will make sure that each of the modules are properly loaded routes.
*/
class ModuleRouteServiceProvider extends RouteServiceProvider
{
    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router $router
     *
     * @return void
     */
    public function map(Router $router)
    {
        // For each of the registered modules, include their routes and Views
        $modules = config("module.modules");

        while (list(,$module) = each($modules)) {

            // Load the routes for each of the modules
            if (file_exists(__DIR__.'/../Modules/'.$module.'/routes.php')) {
                $router->group([], function ($router) use ($module) {
                    require app_path('Modules/' . $module.'/routes.php');
                });
            }
        }
    }
}
