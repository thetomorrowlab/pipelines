<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AdminRequests {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        if (!$user) {
            // If user is not logged in, redirect them to the login screen
            return redirect('/login');
        } elseif (!$user->isAdmin()) {
            $referer = request()->headers->get('referer');
            return redirect($referer ? $referer : '/');
        }

        return $next($request);
    }

}
