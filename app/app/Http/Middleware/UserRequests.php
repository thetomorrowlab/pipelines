<?php

namespace App\Http\Middleware;

use Closure;

class UserRequests {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        if (!$user) {
            // If user is not logged in, redirect them to the login screen
            return redirect('/login');
        }

        return $next($request);
    }

}
