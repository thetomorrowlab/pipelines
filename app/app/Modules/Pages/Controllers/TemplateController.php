<?php
namespace App\Modules\Pages\Controllers;

use App\Modules\Base\Controllers\BaseTemplateController;
use App\Modules\Tasks\Models\Task;

/**
 * TemplateController
 *
 * Controller handles all rendering of templates, related to the displaying
 * of Task database records.
 */
class TemplateController extends BaseTemplateController
{

    public function render($view, $data=[], $statusCode=200)
    {
        $commonData = [];
        $data = array_merge($data, $commonData);

        return parent::render('Pages::' . $view, $data);
    }

    public function homepage()
    {
        $page = [];
        return $this->render('homepage', $page);
    }

    public function generic($slug)
    {
        // Get page by slug
        $page = [];
        return $this->render('generic', $page);
    }
}
