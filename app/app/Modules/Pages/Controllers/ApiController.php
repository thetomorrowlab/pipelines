<?php
namespace App\Modules\Pages\Controllers;

use Validator;
use Illuminate\Http\Request;

use App\Modules\Base\Controllers\BaseApiController;

/**
 * ApiController
 *
 * Controller to handle all interactions between the user and the Task model.
 */
class ApiController extends BaseApiController {

    public function list() {
        return response()->json('list', 200);
    }
    public function create() {
        return response()->json('create', 200);
    }
    public function read() {
        return response()->json('read', 200);
    }
    public function update() {
        return response()->json('update', 200);
    }
    public function delete() {
        return response()->json('delete', 200);
    }
}
