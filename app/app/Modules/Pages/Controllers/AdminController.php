<?php
namespace App\Modules\Pages\Controllers;

use App\Modules\Admin\Controllers\BaseAdminController;

/**
 * TemplateController
 *
 * Controller handles all rendering of templates, related to the displaying
 * of Task database records.
 */
class AdminController extends BaseAdminController
{
    protected $title = 'Pages';
    protected $description = 'Manage all your page content from here';
}
