<html lang="en" ng-app="PublicApp">
    <head>
        <title>Your App</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width,initial-scale=1">

        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=RobotoDraft:300,400,500,700,400italic">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="/css/angular-material.min.css">

        @yield('extra_head')

        <meta name="viewport" content="initial-scale=1" />
    </head>

    <body layout="column" ng-controller="MainCtrl">

        <md-content layout="row" flex class="content-wrapper">

            <div layout="column" flex class="content-wrapper" id="primary-col">

                @include('Pages::partials/header')

                <div layout="column" flex>
                    <md-content>

                        @yield('content')

                    </md-content>
                </div>

            </div>

        </md-content>

        <!-- Third-party -->
        <script src="/js/vendor.js"></script>
        <script src="/js/main.js"></script>

        @yield('extra_js')

    </body>
</html>
