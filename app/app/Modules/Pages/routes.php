<?php
/*
|-----------------------------------------------------------------------------
| Page Module Routes
|-----------------------------------------------------------------------------
*/
Route::group(
    [
        'as'=>'AdminPage::',
        'prefix' => '/admin/pages',
        'namespace' => 'App\Modules\Pages\Controllers',
        'middleware' => ['web', 'auth', 'admin'],
    ],
    function () {
        Route::get('/', [
            'as' => 'list',
            'uses' => 'AdminController@list'
        ]);
        Route::get('/{slug}', [
            'as' => 'detail',
            'uses' => 'AdminController@detail'
        ]);
    }
);

// API endpoints
Route::group(
    [
        'prefix' => 'api/pages',
        'namespace' => 'App\Modules\Pages\Controllers',
        'middleware' => ['api'],
    ],
    function () {
        Route::get('/', 'ApiController@list');

        Route::post('/', 'ApiController@create');
        Route::get('/{id}', 'ApiController@read');
        Route::post('/{id}', 'ApiController@update');
        Route::delete('/{id}/delete', 'ApiController@delete');
    }
);
