@extends('Pages::base')

@section('content')
    <md-card>
        <md-card-title>
            <md-card-title-text>
                <span class="md-headline">Reset Password</span>
            </md-card-title-text>
        </md-card-title>
        <md-card-content>
            <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
            {{ csrf_field() }}
                <md-card>
                    <md-card-content>
                        <md-input-container class="md-block">
                            <label for="email">E-Mail Address</label>
                            <input id="email" name="email" ng-required="true">
                        </md-input-container>
                    </md-card-content>
                </md-card>

                <md-card>
                    <md-card-content>
                        <input type="submit" class="md-button md-raised md-primary" value="Send Password Reset Link">
                    </md-card-content>
                </md-card>
            </form>
        </md-card-content>
    </md-card>

@endsection
