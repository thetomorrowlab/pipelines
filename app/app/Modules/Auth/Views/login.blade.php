@extends('Pages::base')

@section('content')
    <md-card>
        <form class="form-horizontal" method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}
                <md-card>
                    <md-card-content>
                        <md-input-container class="md-block">
                            <label for="email">E-Mail Address</label>
                            <input id="email" name="email" ng-required="true">
                        </md-input-container>

                        <md-input-container class="md-block">
                            <label for="password">Password</label>
                            <input id="password" name="password" ng-required="true" type="password">
                        </md-input-container>

                        <md-input-container class="md-block">
                            <md-checkbox id="remember" name="remember">
                                Remember Me
                            </md-checkbox>
                        </md-input-container>
                    </md-card-content>
                </md-card>

                <md-card>
                    <md-card-content>
                        <input type="submit" class="md-button md-raised md-primary" value="login">
                        <a class="md-button md-raised md-warn" href="{{ route('password.request') }}">
                            Forgot Your Password?
                        </a>
                    </md-card-content>
                </md-card>
            </form>
    </md-card>

@endsection
