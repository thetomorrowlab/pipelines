<?php

namespace App\Modules\Auth\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles()
    {
        return $this
            ->belongsToMany('App\Modules\Auth\Models\Role')
            ->withTimestamps();
    }

    public function isAdmin()
    {
        if ($this->roles()->where('name', 'admin')->count()) {
            return true;
        }
        return false;
    }
}
