<?php

namespace App\Modules\Auth\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public function users()
    {
        return $this
            ->belongsToMany(App\Modules\Auth\Models\User)
            ->withTimestamps();
    }
}
