<?php
namespace App\Modules\Auth\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Modules\Admin\Controllers\BaseAdminController;
use App\Modules\Auth\Models\User;

/**
 * TemplateController
 *
 * Controller handles all rendering of templates, related to the displaying
 * of Task database records.
 */
class AdminController extends BaseAdminController
{

    protected $title = 'Users';
    protected $description = 'Manage site users';
    protected $baseApiRoute = 'users';

    protected function recordExists($id)
    {
        return User::where('id', $id)->exists();
    }
}
