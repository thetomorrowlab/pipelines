<?php
namespace App\Modules\Auth\Controllers;

use App\Modules\Base\Controllers\BaseTemplateController;
use App\Modules\Tasks\Models\Task;

/**
 * TemplateController
 *
 * Controller handles all rendering of templates, related to the displaying
 * of Task database records.
 */
class TemplateController extends BaseTemplateController
{

    public function profile()
    {
        return $this->render('Auth::profile');
    }
}
