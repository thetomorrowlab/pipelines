<?php
namespace App\Modules\Auth\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\Modules\Base\Controllers\ModelApiController;
use App\Modules\Auth\Serialisers\UserSerialiser;

/**
 * ApiController
 *
 * Controller to handle all interactions between the user and the Task model.
 */
class ApiController extends ModelApiController {

    public function __construct()
    {
        $this->serilaiser = new UserSerialiser();
    }

}
