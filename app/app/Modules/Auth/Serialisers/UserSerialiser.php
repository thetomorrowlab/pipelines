<?php

namespace App\Modules\Auth\Serialisers;

use App\Modules\Base\Serialisers\ModelSerialiser;
use App\Modules\Auth\Models\User;

class UserSerialiser extends ModelSerialiser
{

    protected $modelNS = 'App\Modules\Auth\Models\User';
    protected $singularType = 'user';
    protected $pluralType = 'users';
}
