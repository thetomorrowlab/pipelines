<?php

/*
|--------------------------------------------------------------------------
| User Auth Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
|
*/

Route::group(
    [
        'as'=>'',
        'prefix' => '/auth',
        'namespace' => 'App\Modules\Auth\Controllers',
        'middleware' => ['web'],
    ],
    function () {
        Route::get('/login', ['as' => 'login', 'uses' => 'LoginController@showLoginForm']);
        Route::post('/login', 'LoginController@login');
        Route::post('/logout', ['as' => 'logout', 'uses' => 'LoginController@logout']);
    }
);

Route::group(
    [
        'as'=>'',
        'prefix' => '/auth',
        'namespace' => 'App\Modules\Auth\Controllers',
        'middleware' => ['web'],
    ],
    function () {
        Route::get('/register', ['as' => 'register', 'uses' => 'RegisterController@showRegistrationForm']);
        Route::post('/register', 'RegisterController@register');
    }
);

Route::group(
    [
        'as'=>'',
        'prefix' => '/auth/password',
        'namespace' => 'App\Modules\Auth\Controllers',
        'middleware' => ['web'],
    ],
    function () {
        Route::get('/reset', ['as' => 'password.request', 'uses' => 'ForgotPasswordController@showLinkRequestForm']);
        Route::post('/email', ['as' => 'password.email', 'uses' => 'ForgotPasswordController@sendResetLinkEmail']);
    }
);

Route::group(
    [
        'as'=>'',
        'prefix' => '/auth/password/reset',
        'namespace' => 'App\Modules\Auth\Controllers',
        'middleware' => ['web'],
    ],
    function () {
        Route::get('/{token}', ['as' => 'password.reset', 'uses' => 'ResetPasswordController@showResetForm']);
        Route::post('', 'ResetPasswordController@reset');
    }
);

Route::group(
    [
        'as'=>'AdminUser::',
        'prefix' => '/admin/users',
        'namespace' => 'App\Modules\Auth\Controllers',
        'middleware' => ['web', 'auth', 'admin'],
    ],
    function () {
        Route::get('/', [
            'as' => 'list',
            'uses' => 'AdminController@list'
        ]);
        Route::get('/{slug}', [
            'as' => 'detail',
            'uses' => 'AdminController@detail'
        ]);
    }
);

// API endpoints
Route::group(
    [
        'prefix' => 'api/v' . env('API_VERSION') . '/users',
        'namespace' => 'App\Modules\Auth\Controllers',
        'middleware' => ['api'],
    ],
    function () {
        Route::get('/', 'ApiController@list');

        Route::post('/', 'ApiController@create');
        Route::get('/{id}', 'ApiController@read');
        Route::post('/{id}', 'ApiController@update');
        Route::delete('/{id}/delete', 'ApiController@delete');
    }
);
