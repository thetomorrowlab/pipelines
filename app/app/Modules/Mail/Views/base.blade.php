<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    @include('Mail::styles.default_css')
</head>
<body bgcolor="#FFF" style="background-color: #FFF">

<table class="body-wrap" width="100%" cellpadding="0" cellspacing="20">
    <tr bgcolor="#FFF">
        <td class="container">
            <div class="content">
                <table bgcolor="#FFF" width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            @yield('content')
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
</table>
</body>
</html>
