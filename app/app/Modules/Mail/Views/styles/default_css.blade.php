<style>
    /* -------------------------------------
     GLOBAL
    ------------------------------------- */
    * {
        margin:0;
        padding:0;
    }

    body {
        -webkit-font-smoothing:antialiased;
        -webkit-text-size-adjust:none;
        width: 100%!important;
        height: 100%;
    }
</style>
