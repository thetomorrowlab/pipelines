<?php

namespace App\Modules\Base\Serialisers;

class BaseSerialiser
{
    // Data Descriptors
    protected $singularType;
    protected $pluralType;
}
