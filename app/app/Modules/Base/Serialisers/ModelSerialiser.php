<?php

namespace App\Modules\Base\Serialisers;


class ModelSerialiser extends BaseSerialiser
{
    // Model to use for queries
    protected $modelNS;

    // ID of active record
    private $activeId;
    private $record;

    public function __construct()
    {
        $this->activeId = request()->route('id');
    }

    public function getRecord()
    {
        return $this->modelNS::find($this->activeId);
    }

    public function list()
    {
        return [
            $this->pluralType => $this->modelNS::get()->toArray()
        ];
    }

    public function detail()
    {
        $record = $this->getRecord();
        if ($record) {
            return [
                $this->singularType => $record->toArray()
            ];
        }
        return false;
    }

    public function create()
    {
        $this->record = $this->modelNS::create();
    }

    public function update()
    {
        $this->record = $this->modelNS::update();
    }

    public function delete()
    {
        try {
            $this->modelNS::delete();
            return true;
        } catch (Exception $e) {}
        return false;
    }
}
