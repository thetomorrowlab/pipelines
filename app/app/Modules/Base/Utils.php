<?php namespace App\Modules\Base;

class Utils
{
    /**
     * Convert camelCase and _underscore_ strings into a human readable format
     *
     * Regex taken from:
     * https://stackoverflow.com/questions/4519739
     */
    public static function humanise($string, $capFirst=false, $capAll=false)
    {

        $string = trim(str_replace('_', ' ', $string));

        $re = '/(?#! splitCamelCase Rev:20140412)
            # Split camelCase "words". Two global alternatives. Either g1of2:
              (?<=[a-z])      # Position is after a lowercase,
              (?=[A-Z])       # and before an uppercase letter.
            | (?<=[A-Z])      # Or g2of2; Position is after uppercase,
              (?=[A-Z][a-z])  # and before upper-then-lower case.
            /x';

        $words = preg_split($re, $string);

        if ($capFirst) {
            return ucfirst(implode(' ', $words));
        }

        if ($capAll) {
            $words = array_map('ucwords', $words);
        }

        return implode(' ', $words);
    }
}
