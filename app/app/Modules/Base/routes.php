<?php
/*
|-----------------------------------------------------------------------------
| Base Module Routes
|-----------------------------------------------------------------------------
*/

// Web views
Route::group(
    [
        'prefix' => '',
        'namespace' => 'App\Modules\Base\Controllers'
    ],
    function () {
        Route::post('/csp', 'BaseController@logCspViolation');
    }
);
