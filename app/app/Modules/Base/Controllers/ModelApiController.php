<?php

namespace App\Modules\Base\Controllers;

use Illuminate\Http\Request;

/**
 * BaseApiController
 *
 * Core functionality for all api endpoints that manage database models
 */
class ModelApiController extends BaseApiController
{

    protected $serilaiser;
    protected $availableMethods = ['list', 'create', 'read', 'update', 'delete'];

    public function list(Request $request) {
        return $this->render([
            'data' => $this->serilaiser->list()
        ]);
    }

    public function create(Request $request) {
        if ($this->serilaiser->createeate()) {
            return $this->render([
                'data' => $this->serilaiser->detail()
            ]);
        }
        $this->renderErrors();
    }

    public function read(Request $request, $recordId) {
        return $this->render([
            'data' => $this->serilaiser->detail()
        ]);
        $this->renderErrors();
    }

    public function update(Request $request, $recordId) {
        if ($this->serilaiser->update()) {
            return $this->render([
                'data' => $this->serilaiser->detail()
            ]);
        }
        $this->renderErrors();
    }

    public function delete(Request $request, $recordId) {
        if ($this->serilaiser->delete()) {
            return $this->render([
                'data' => NULL
            ]);
        }
        $this->renderErrors();
    }

    private function renderErrors()
    {
        return $this->render([
            'status' => 400,
            'data' => $this->serilaiser->errors()
        ]);
    }

    private function invalidMethod($method)
    {
        return $this->render([
            'status' => 403,
            'message' => 'Method not allowed'
        ]);
    }

}
