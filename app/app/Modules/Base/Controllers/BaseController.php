<?php

namespace App\Modules\Base\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Log;

/**
 * BaseController
 *
 * Core functionality for all controllers
 */
class BaseController extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $nonceToken;

    public function __construct()
    {
        $this->setCspNonce();
    }

    /**
      * Creates and sets a nonce token used for the csp headers
      * Unique token generated per request
      *
      * @return void
      */
    private function setCspNonce()
    {
        $this->nonceToken = substr(base64_encode(bin2hex(random_bytes(10 * 2))), 0, 10);
    }

    /**
      * Generates all the common response headers for all responses from the app
      *
      * @return array
      */
    protected function getCommonResponseHeaders()
    {
        $headers['X-Frame-Options'] = 'DENY';
        $headers['X-Content-Type-Options'] = 'nosniff';
        $headers['X-XSS-Protection'] = '1; mode=block';
        $headers['Strict-Transport-Security'] = 'max-age=2592000; includeSubdomains';

        $cspHeader = 'Content-Security-Policy';
        if (config('csp.report_only')) {
            $cspHeader = 'Content-Security-Policy-Report-Only';
        }
        $directives = [];
        foreach (config('csp.policy') as $key => $value) {
            $directive = join(' ', [$key, $value]);
            if ($key === 'script-src') {
                $directive = join('', [$directive, ' \'nonce-', $this->nonceToken, '\'']);
            }
            $directives[] = $directive;
        }
        $headers[$cspHeader] = join('; ', $directives);

        return $headers;
    }

    protected function getApiPath($module = '')
    {
        return sprintf('%s/api/v%d/%s', request()->root(), env('API_VERSION', 1), $module);
    }

    public function logCspViolation(Request $request)
    {
        Log::warning($request->get('csp-report'));
    }
}
