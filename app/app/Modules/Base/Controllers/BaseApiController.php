<?php

namespace App\Modules\Base\Controllers;

use Illuminate\Http\Request;

/**
 * BaseApiController
 *
 * Core functionality for all api controllers
 *
 * All responses should flow the JSend format. JSend format has been chosen
 * for its simplicity.
 * https://labs.omniti.com/labs/jsend
 */
class BaseApiController extends BaseController
{
    /**
     * Extends the common response headers to include json specific headers
     *
     * @return array
     */
    protected function getCommonResponseHeaders()
    {
        $headers = parent::getCommonResponseHeaders();
        $headers['Content-Type'] = 'application/json; charset=utf-8';

        if (env('APP_ENV') !== 'local') {
            $headers['Content-Disposition'] = 'attachment; filename=json';
        }
        return $headers;
    }

    /**
     * Returns a response object with json headers set
     *
     * @param array $data Template data
     * @param int $statusCode Response status code
     *
     * @return Response obj
     */
    public function render($responseData)
    {
        $statusCode = 200;
        if (in_array('status', $responseData)) {
            $statusCode = $responseData['status'];
        }

        $response = [];
        if ($statusCode === 200) {
            $response['status'] = 'success';
            $response['data'] = $responseData['data'];
        } elseif ($statusCode === 400) {
            // Rejected due to invalid data or call conditions
            $response['status'] = 'fail';
            $response['data'] = $responseData['data'];
        } else {
            // Rejected due to an error on the server
            $response['status'] = 'error';
            $response['message'] = $responseData['message'];

            if (in_array('data', $responseData)) {
                $response['data'] = $responseData['data'];
            }

            if (in_array('code', $responseData)) {
                $response['code'] = $responseData['code'];
            }
        }

        return response()
            ->json($response, $statusCode)
            ->withHeaders($this->getCommonResponseHeaders());
    }
}
