<?php

namespace App\Modules\Base\Controllers;

/**
 * BaseTemplateController
 *
 * Core functionality for all template controllers
 */
class BaseTemplateController extends BaseController
{
    const SESSION_FLASH_MESSAGE_KEY = 'session_flash_message_key';

    public $flashMessages = [];

     /**
      * Sets a flash message for the current response
      *
      * @param string $message Content of the flash message
      * @param string $level class used for displaying the message
      *     success - green
      *     info    - blue
      *     warning - orange
      *     danger  - red
      *
      * @return void
      */
    public function setFlashMessage($message, $level='success')
    {
        $this->flashMessages[] = [$level => $message];
    }

    /**
      * Sets a flash message for the next response
      *
      * @param string $message Content of the flash message
      * @param string $level class used for displaying the message
      *     success - green
      *     info    - blue
      *     warning - orange
      *     danger  - red
      *
      * @return void
      */
    public function setSessionFlashMessage($message, $level = 'success')
    {
        session()->push($this::SESSION_FLASH_MESSAGE_KEY, [$level => $message]);
    }

    /**
      * Retrieves all flash messages for the current response
      *
      * @return array
      */
    public function getFlashMessages()
    {
        $sessionMessages = session()->pull($this::SESSION_FLASH_MESSAGE_KEY, []);
        return array_merge($sessionMessages, $this->flashMessages);
    }

    /**
      * Returns a response object with common template variables and headers
      *
      * @param string $view View template to use
      * @param array $data Template data
      * @param int $statusCode Response status code
      *
      * @return Response obj
      */
    public function render($view, $data = [], $statusCode = 200)
    {
        $data['appName'] = config('app.name');
        $data['csp_nonce'] = $this->nonceToken;
        $data['basePath'] = request()->url();
        return response()
            ->view($view, $data, $statusCode)
            ->withHeaders($this->getCommonResponseHeaders());
    }

    /**
      * Returns a parsed view template as a string
      *
      * @param string $view View template to use
      * @param array $data Template data
      *
      * @return string
      */
    public function reponseToString($view, $data = [])
    {
        return $this->render($view, $data)->render();
    }
}
