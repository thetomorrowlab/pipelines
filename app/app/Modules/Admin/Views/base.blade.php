<html lang="en" ng-app="AdminApp">
    <head>
        <title>{{ $title }} - {{ $appName }}</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width,initial-scale=1">

        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=RobotoDraft:300,400,500,700,400italic">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="/css/angular-material.min.css">

        @yield('extra_head')

        <meta name="viewport" content="initial-scale=1" />
    </head>

    <body layout="column">

        <md-content layout="row" flex class="content-wrapper" ng-controller="MainCtrl" ng-cloak>

            @include('Admin::partials/sidebar')

            <div layout="column" flex class="content-wrapper" id="primary-col">

                @include('Admin::partials/header')

                <div layout="column" flex>
                    <md-content>

                        <div class="heading-placeholder">
                            <h2 class="md-display-2">{{ $title }}</h2>
                            <p class="md-subhead">{{ $description }}</p>
                        </div>

                        @yield('content')

                    </md-content>
                </div>

            </div>

        </md-content>

        <!-- Third-party -->
        <script src="/js/vendor.js"></script>
        <script src="/js/admin.js"></script>

        @yield('extra_js')

    </body>
</html>
