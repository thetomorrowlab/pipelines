<md-sidenav class="md-sidenav-left md-whiteframe-z2" md-component-id="left" md-is-locked-open="$mdMedia('gt-md')">
    <div layout="column">
        <div flex>
            <md-content>
                <md-list flex>

                    @foreach($navItems as $item)

                        <md-list-item class="md-1-line" ng-click="goTo('{{$item['url']}}')">
                            <div class="md-list-item-text" layout="column">
                                {{$item['title']}}
                            </div>
                        </md-list-item>

                    @endforeach

                </md-list>
            </md-content>
        </div>
    </div>
</md-sidenav>
