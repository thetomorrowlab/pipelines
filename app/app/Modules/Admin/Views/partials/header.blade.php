<md-toolbar>
    <div class="md-toolbar-tools">
        <md-button class="md-icon-button" aria-label="Toggle Menu" hide-gt-md ng-click="toggleSideBar()">
            <i class="material-icons">menu_white</i>
        </md-button>
        <h2 flex>
            <span>{{$appName}} Content Management System</span>
        </h2>
        <md-button href="{{ url('/logout') }}">
            Log out
        </md-button>
    </div>
</md-toolbar>
