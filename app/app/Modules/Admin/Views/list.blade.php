@extends('Admin::base')

@section('content')
    <div ng-init="apiPath='{{$apiPath}}'"></div>

    <md-list ng-controller="ListCtrl" ng-cloak>
        <md-subheader class="md-no-sticky">Users</md-subheader>
        <md-list-item ng-repeat="user in data.users">
            <p>{[ user.name ]}</p>
            <a href="{{$basePath}}/{[user.id]}">
                <i class="material-icons">edit</i>
            </a>
        </md-list-item>
    </md-list>
@endsection
