<?php
/*
|-----------------------------------------------------------------------------
| Admin Module Routes
|-----------------------------------------------------------------------------
*/

// Admin section has no base page, so redirect users to the pages module

Route::group(
    [
        'middleware' => ['web', 'auth', 'admin'],
    ],
    function () {
        Route::get('/admin', function(Request $request) {
            return redirect(route('AdminPage::list'));
        });
    }
);
