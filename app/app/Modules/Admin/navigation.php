<?php

return [
    [
        'title' => 'Users',
        'url' => route('AdminUser::list'),
    ],
    [
        'title' => 'Pages',
        'url' => route('AdminPage::list'),
    ],
];
