<?php
namespace App\Modules\Admin\Controllers;

use Illuminate\Http\Request;
use App\Modules\Base\Controllers\BaseTemplateController;
use App\Modules\Admin\Admin;

/**
 * BaseAdminController
 *
 * Controller handles all rendering of templates, related to the displaying
 * of Task database records.
 */
class BaseAdminController extends BaseTemplateController
{
    // Page title and description
    protected $title;
    protected $description;
    protected $listViewTemplate = 'Admin::list';
    protected $detailViewTemplate = 'Admin::detail';

    // Model used to preform queries against
    protected $model;

    // Path path to preform CRUD commands
    protected $baseApiRoute;

    public function list(Request $request)
    {
        return $this->render($this->listViewTemplate);
    }

    public function detail(Request $request, $id)
    {
        if ($id !== 'new' && !$this->recordExists($id)) {
            abort(404);
        }
        return $this->render($this->detailViewTemplate);
    }

    public function render($view, $data = [], $statusCode = 200)
    {
        $data['title'] = $this->title;
        $data['description'] = $this->description;
        $data['navItems'] = $this->getNavItems();
        $data['apiPath'] = $this->getApiPath($this->baseApiRoute);
        return parent::render($view, $data, $statusCode);
    }

    private function getNavItems()
    {
        return include(__DIR__.'/../navigation.php');
    }
}
