<?php
/*
|-----------------------------------------------------------------------------
| Homepage Routes
|-----------------------------------------------------------------------------
*/

// Web views
Route::group(
    [
        'prefix' => '',
        'namespace' => 'App\Modules\Homepage\Controllers',
        'middleware' => ['web'],
    ],
    function () {
        Route::get('/', 'TemplateController@homepage');
    }
);

Route::get('/send-email', 'App\Modules\Homepage\Controllers\EmailController@send');
