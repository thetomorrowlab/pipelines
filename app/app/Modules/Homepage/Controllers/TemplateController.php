<?php
namespace App\Modules\Homepage\Controllers;

use App\Modules\Base\Controllers\BaseTemplateController;

/**
 * TemplateController
 *
 * Controller handles all rendering of templates, related to the displaying
 * of Task database records.
 */
class TemplateController extends BaseTemplateController
{

    public function homepage()
    {
        return $this->render('Homepage::index');
    }
}
