<?php
namespace App\Modules\Homepage\Controllers;

use Illuminate\Support\Facades\Mail;

use App\Modules\Base\Controllers\BaseController;
use App\Modules\Homepage\Mail\TestMail;

/**
 * TemplateController
 *
 * Controller handles all rendering of templates, related to the displaying
 * of Task database records.
 */
class EmailController extends BaseController
{
    public function send()
    {
        Mail::to('test@example.com')->send(new TestMail());

        return redirect('/');
    }
}
