@extends('Mail::base')

@section('content')

    <p>This is a test email</p>

    <table class="action" width="100%" align="center" cellpadding="0" cellspacing="10">
        <tr align="center">
            <td align="center">
                <table bgcolor="#FFF" cellpadding="7" cellspacing="7">
                    <tr bgcolor="#FFF">
                        <td bgcolor="#FFF">
                            <a style="font-weight: bold; text-decoration: none" align="center" href="{{url('/')}}" target="_blank">
                                LINK BACK TO SITE
                            </a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

@endsection
