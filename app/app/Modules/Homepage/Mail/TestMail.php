<?php

namespace App\Modules\Homepage\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\App;

class TestMail extends Mailable
{
    use Queueable;

    /*
     * Sends a compiled view template as an HTML email
     *
     * A text version is also attached, using the Html2Text package.
     *
     */
    public function build()
    {
        $content = view('Homepage::mail.test')->render();

        return $this->subject('Test Email')
            ->view('Mail::raw', [
                'content' => $content
            ])
            ->text('Mail::text')->with([
                'textContent' => @\Html2Text\Html2Text::convert($content)
            ]);
    }
}
