<?php

return  [
    'modules' => [
        'Auth',
        'Admin',
        'Base',
        'Homepage',
        'Mail',
        'Pages'
    ]
];
