<?php

return [
    'policy' => [
        # Restrict base tags to same origin, to prevent CSP bypasses.
        'base-uri' => '\'self\'',
        # Disallow Flash, etc.
        'object-src' => '\'none\'',
        'default-src' => '\'none\'',
        'script-src' => '\'self\' \'unsafe-inline\' https: http:',
        'style-src' => '\'self\' https://fonts.googleapis.com \'unsafe-inline\'',
        'img-src' => '\'self\' data:',
        'font-src' => '\'self\' https://fonts.gstatic.com',
        'report-uri' => '/csp',
        'connect-src' => '\'self\'',
        'frame-src' => '\'self\''
    ],
    'report_only' => env('APP_DEBUG', false)
];
