# Docker Pipelines image

***

This is the source Dockerfile for the image hosted at https://hub.docker.com/r/tomorrowlabmark/forge-node

This image is used to provide a basic web server setup to interact with:

    - PHP (Laravel webframework)
    - Composer
    - Redis
    - NodeJS
    - MariaDB Database

## Docker Hub
***

All images for CI are hosted [Docker hub](https://hub.docker.com/).

To build the image from the `Dockerfile`, run:

    docker build . --tag tomorrowlabmark/[name]:[version]

You should always have a `latest` verison as well as specific versions where possible.

After building the image, you can view it via `docker images`, you should see:

    REPOSITORY                      TAG                 IMAGE ID            CREATED             SIZE
    [name]                          [tag]               [random ID]         X seconds ago       539MB

You can then push the built image up to docker hub via:

    docker push tomorrowlabmark/[repo-name]:[tag]

