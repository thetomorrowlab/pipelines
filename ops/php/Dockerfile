FROM php:7.1-fpm
MAINTAINER Mark McConnell <mark@thetomorrowlab.com>

# keep upstart quiet
RUN dpkg-divert --local --rename --add /sbin/initctl
RUN ln -sf /bin/true /sbin/initctl

# no tty
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get install -y \
    cron \
    curl \
    libmcrypt-dev \
    libpq-dev \
    libcurl4-gnutls-dev \
    zip \
    unzip && docker-php-ext-install -j$(nproc) pdo_mysql

# XDebug
COPY ./scripts/docker-php-pecl-install /usr/local/bin/
RUN chmod +x /usr/local/bin/docker-php-pecl-install
RUN docker-php-pecl-install xdebug-2.5.5

COPY ./scripts/xdebug.ini /etc/php/7.0/mods-available/xdebug.ini

# Log php info out to docker stdout
ADD ./scripts/log.conf /usr/local/etc/php-fpm.d/zz-log.conf

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
